fpylll (0.6.3-2) unstable; urgency=medium

  * Team upload.
  * Build-depend on dh-sequence-numpy3 (closes: #1094910).

 -- Colin Watson <cjwatson@debian.org>  Mon, 03 Feb 2025 15:10:09 +0000

fpylll (0.6.3-1) unstable; urgency=medium

  * New upstream release.

 -- Julien Puydt <jpuydt@debian.org>  Fri, 24 Jan 2025 09:21:53 +0100

fpylll (0.6.2-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 0.6.2
    - New version fixes FTBFS. (Closes: #1088361)

 -- Sudip Mukherjee <sudipm.mukherjee@gmail.com>  Thu, 12 Dec 2024 22:11:17 +0000

fpylll (0.6.1-2) unstable; urgency=medium

  * Team upload.
  * debian/control: Use canonical maintainer name and email.
  * debian/control: Replace pkg-config with pkgconf.
  * debian/control: Bump Standards-Version to 4.7.0.
  * debian/: Use dh-sequence-python3.

 -- Boyuan Yang <byang@debian.org>  Fri, 11 Oct 2024 11:41:19 -0400

fpylll (0.6.1-1) unstable; urgency=medium

  * New upstream release.

 -- Julien Puydt <jpuydt@debian.org>  Thu, 25 Jan 2024 13:37:09 +0100

fpylll (0.6.0-1) unstable; urgency=medium

  [ Julien Puydt ]
  * New upstream release (Closes: #1055732, #1056800).
  * Fix clean target (Closes: #1044980).

 -- Alexandre Detiste <tchet@debian.org>  Tue, 16 Jan 2024 22:52:13 +0100

fpylll (0.5.9-1) unstable; urgency=medium

  * New upstream release.

 -- Julien Puydt <jpuydt@debian.org>  Mon, 16 Jan 2023 10:53:20 +0100

fpylll (0.5.8-1) unstable; urgency=medium

  * New upstream release.
  * Drop now-useless patch.
  * Bump standards-version to 4.6.2.
  * Add patch to find default strategies.

 -- Julien Puydt <jpuydt@debian.org>  Thu, 12 Jan 2023 17:39:36 +0100

fpylll (0.5.7-1) unstable; urgency=medium

  * Fix d/watch again.
  * Upstream source doesn't contain excluded files anymore.
  * New upstream release.
  * Drop now-useless patch.
  * Add patch to find default.json.

 -- Julien Puydt <jpuydt@debian.org>  Sat, 29 Oct 2022 07:46:05 +0200

fpylll (0.5.2+ds1-9) unstable; urgency=medium

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster (oldstable):
    + Build-Depends: Drop versioned constraint on cython3.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Fri, 28 Oct 2022 19:20:50 +0100

fpylll (0.5.2+ds1-8) unstable; urgency=medium

  [ Debian Janitor ]
  * Update renamed lintian tag names in lintian overrides.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sun, 16 Oct 2022 18:24:49 +0100

fpylll (0.5.2+ds1-7) unstable; urgency=medium

  * Fix d/watch.

 -- Julien Puydt <jpuydt@debian.org>  Sun, 02 Oct 2022 21:58:46 +0200

fpylll (0.5.2+ds1-6) unstable; urgency=medium

  * Bump fplll's version in b-dep since autopkgtest
  fails with the old one.

 -- Julien Puydt <jpuydt@debian.org>  Sat, 25 Jun 2022 08:15:28 +0200

fpylll (0.5.2+ds1-5) unstable; urgency=medium

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster:
    + python3-fpylll: Drop versioned constraint on sagemath in Breaks.

  [ Julien Puydt ]
  * Bump standards-version to 4.6.1.

 -- Julien Puydt <jpuydt@debian.org>  Wed, 22 Jun 2022 15:35:49 +0200

fpylll (0.5.2+ds1-4) unstable; urgency=medium

  * Team upload.
  * debian/control
    - use the proper team email address

 -- Sandro Tosi <morph@debian.org>  Wed, 25 Aug 2021 00:01:56 -0400

fpylll (0.5.2+ds1-3) unstable; urgency=medium

  * Add patch from upstream. (Closes: #976778)
  * Bump std-vers to 4.5.1.

 -- Julien Puydt <jpuydt@debian.org>  Sat, 19 Dec 2020 11:59:47 +0100

fpylll (0.5.2+ds1-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Julien Puydt ]
  * Drop unneeded lintian overrides.

 -- Julien Puydt <jpuydt@debian.org>  Mon, 12 Oct 2020 22:44:41 +0200

fpylll (0.5.2+ds1-1) unstable; urgency=medium

  * New upstream release.
  * Bump dh-compat to 13.

 -- Julien Puydt <jpuydt@debian.org>  Sun, 02 Aug 2020 08:42:39 +0200

fpylll (0.5.1+ds1-3) unstable; urgency=medium

  * Fix broken symlink from the -doc package.
  * Bump dh-compat to 12.
  * Bump std-vers to 4.5.0.
  * Point to the new DPMT address.
  * Add d/upstream/metadata.
  * Add Rules-Requires-Root: no.

 -- Julien Puydt <jpuydt@debian.org>  Wed, 17 Jun 2020 23:00:02 +0200

fpylll (0.5.1+ds1-2) unstable; urgency=medium

  * Team upload.
  * debian/control
    - b-d-i on python3-docutils

 -- Sandro Tosi <morph@debian.org>  Sat, 18 Jan 2020 17:47:29 -0500

fpylll (0.5.1+ds1-1) unstable; urgency=medium

  * New upstream release.
  * Drop time patch, upstreamed.
  * Bump libfplll-dev dep to 5.3.0 (new pruner.h).
  * Add build-dep on libpari-dev.
  * Drop M-A: same since lintian complains.
  * Unbreak autopkgtest.

 -- Julien Puydt <jpuydt@debian.org>  Fri, 20 Dec 2019 08:44:50 +0100

fpylll (0.4.1+ds1-7) unstable; urgency=medium

  * Team upload
  * Fix time-clock-deprecation in Python 3.8
    - Apply Matthias Klose patch (Closes: #943324)
  * d/control: Bump Standard-Version to 4.4.1

 -- Emmanuel Arias <emmanuelarias30@gmail.com>  Wed, 23 Oct 2019 23:17:18 -0300

fpylll (0.4.1+ds1-6) unstable; urgency=medium

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.
  * d/control: Remove ancient X-Python3-Version field.

  [ Sandro Tosi ]
  * Drop python2 support; Closes: #936549

 -- Sandro Tosi <morph@debian.org>  Wed, 16 Oct 2019 23:49:20 -0400

fpylll (0.4.1+ds1-5) unstable; urgency=medium

  * Team upload.
  * wrap-and-sort -ast.
  * Properly build-depend on python(3)-all-dev instead of libpython(3.X)-dev.

 -- Mattia Rizzolo <mattia@debian.org>  Wed, 19 Dec 2018 02:34:20 +0100

fpylll (0.4.1+ds1-4) unstable; urgency=medium

  * Bug fix release (Closes: #891593), refresh d/*.links .
  * Debianization:
    - debian/control:
      - Standards-Version, bump to 4.2.1 (no change).

 -- Jerome Benoit <calculus@rezozer.net>  Fri, 05 Oct 2018 07:50:51 +0000

fpylll (0.4.1+ds1-3) unstable; urgency=medium

  * Team upload.
  * Upload to unstable.

 -- Tobias Hansen <thansen@debian.org>  Wed, 26 Sep 2018 20:08:00 +0200

fpylll (0.4.1+ds1-2) experimental; urgency=medium

  * Team upload.

  [ Ondřej Nový ]
  * Convert git repository from git-dpm to gbp layout

  [ Tobias Hansen ]
  * Build-Depend on fplll 5.2.1. (Closes: #905434)

 -- Tobias Hansen <thansen@debian.org>  Tue, 21 Aug 2018 10:50:26 +0200

fpylll (0.4.1+ds1-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * d/control: Remove ancient X-Python-Version field

  [ Julien Puydt ]
  * New upstream release.
  * Drop all patches : upstreamed!

 -- Julien Puydt <jpuydt@debian.org>  Wed, 25 Jul 2018 10:30:15 +0200

fpylll (0.3.0+ds1-4) experimental; urgency=medium

  * RC fix release (Closes: #889584), apply upstream patch.

 -- Jerome Benoit <calculus@rezozer.net>  Fri, 16 Feb 2018 13:10:29 +0000

fpylll (0.3.0+ds1-3) experimental; urgency=medium

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Fix Format URL to correct one

 -- Jerome Benoit <calculus@rezozer.net>  Fri, 16 Feb 2018 12:31:29 +0000

fpylll (0.3.0+ds1-2) experimental; urgency=medium

  * RC fix release (Closes: #888758), complete copyright for s/f/gmp/* .

 -- Jerome Benoit <calculus@rezozer.net>  Fri, 02 Feb 2018 14:59:25 +0000

fpylll (0.3.0+ds1-1) experimental; urgency=medium

  * Debianization:
    - add Python 3 support (Closes: #874696);
    - debian/copyright:
      - copyright year tuple, refresh;
      - Files-Excluded field, update;
    - debian/watch:
      - options, add compression;
    - debian/control:
      - debhelper, bump to 11;
      - Standards-Version, bump to 4.1.3;
      - Build-Depends, refresh;
    - debian/rules:
      - get-orig-source target, compression option, remove;
    - debian/patches/*:
      - d/p/upstream-fix-lintian-spelling-error.patch , introduce;
    - debian/tests/*:
      - refresh.

 -- Jerome Benoit <calculus@rezozer.net>  Fri, 26 Jan 2018 14:25:40 +0000

fpylll (0.3.0+ds-3) unstable; urgency=medium

  * Team upload.
  * Upload to unstable.
  * Add Breaks: sagemath (<< 8.1~) to ensure fpylll migrates to testing
    together with sagemath 8.1.

 -- Tobias Hansen <thansen@debian.org>  Sat, 30 Dec 2017 11:47:31 +0100

fpylll (0.3.0+ds-2) experimental; urgency=medium

  * Work around upstream #112 test failure on 32-bit arches. (Closes: #881878)

 -- Ximin Luo <infinity0@debian.org>  Thu, 28 Dec 2017 13:46:36 +0100

fpylll (0.3.0+ds-1) experimental; urgency=medium

  * Team upload to experimental.

  [ Julien Puydt ]
  * New upstream release.
  * Bump std-ver to 4.1.1.
  * Mark the -doc as Multi-Arch: foreign following hinter.
  * Drop the now-upstreamed patch for more robustness when importing.

  [ Tobias Hansen ]
  * Build-Depend on libfplll-dev (>= 5.2.0).

 -- Tobias Hansen <thansen@debian.org>  Sun, 12 Nov 2017 16:16:37 +0100

fpylll (0.2.4+ds-3) unstable; urgency=medium

  * Upload to unstable.

 -- Ximin Luo <infinity0@debian.org>  Sun, 27 Aug 2017 14:20:29 +0200

fpylll (0.2.4+ds-2) experimental; urgency=medium

  * Be more robust to different types of errors when importing Sage. See Github
    PR#97 for details.
  * Add myself to Uploaders.
  * Update to latest Standards-Version; no changes required.

 -- Ximin Luo <infinity0@debian.org>  Wed, 16 Aug 2017 23:19:21 +0200

fpylll (0.2.4+ds-1) experimental; urgency=medium

  * New upstream release.
  * Debianization:
    - debian/copyright, update;
    - debian/patches/*, integrated --- thanks to the upstream maintainer
      Martin Albrecht <martinralbrecht+github@googlemail.com>.

 -- Jerome Benoit <calculus@rezozer.net>  Fri, 16 Jun 2017 07:21:06 +0000

fpylll (0.2.3+ds-2) unstable; urgency=medium

  * Debianization:
    - debian/control:
      - [Build-]Depends field, specify a default favour for python-cysignals;
      - Depends field for python-fpylll, add python-cysignals,numpy}, thanks
        to post-install tests;
    - debian/tests/control:
      - py.test, now performed for each favour of python-cysignals.

 -- Jerome Benoit <calculus@rezozer.net>  Sat, 05 Nov 2016 03:28:31 +0000

fpylll (0.2.3+ds-1) unstable; urgency=medium

  * Initial release. (Closes: #841005)

 -- Jerome Benoit <calculus@rezozer.net>  Sat, 29 Oct 2016 14:09:55 +0000
